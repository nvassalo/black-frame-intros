THREE.LaserShader = {

    uniforms: {
        "uColor": {type: "c", value: new THREE.Color(0, 0, 0)},
        "uMultiplier": {type: "f", value: 1.1},
        "uCoreColor": {type: "c", value: new THREE.Color(1, 1, 1)},
        "uCoreOpacity": {type: "f", value: 1},
        "uLowerBound": {type: "f", value: 0.1},
        "uUpperBound": {type: "f", value: 100},
        "uTransitionPower": {type: "f", value: 100},
        "uNearPlaneValue": {type: "f", value: -0.01},
        "vFactor": {type: "f", value: 0.1}
    },

    vertexShader: [
        "varying vec2 vUv;",
        "varying float vFactor;",
        "void main() {",
        "vUv = uv;",
        "gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
        "}"

    ].join("\n"),

    fragmentShader: [

        "uniform vec3 uColor;",
        "uniform vec3 uCoreColor;",
        "uniform float uCoreOpacity;",
        "uniform float uLowerBound;",
        "uniform float uUpperBound;",
        "uniform float uTransitionPower;",
        "varying float vFactor;",

        "void main() {",

        "vec4 col = vec4(uColor, vFactor);",
        "float factor = smoothstep(uLowerBound, uUpperBound, vFactor);",
        "factor = pow(factor, uTransitionPower);",
        "vec4 coreCol = vec4(uCoreColor, uCoreOpacity);",
        "vec4 finalCol = mix(col, coreCol, factor);",
        "gl_FragColor = finalCol;",

        "}"

    ].join( "\n" )

};