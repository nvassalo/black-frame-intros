/**
 * @author alteredq / http://alteredqualia.com/
 *
 * Full-screen textured quad shader
 */

THREE.GlowShader = {

	uniforms: {
		"viewVector": { type: "v3", value: [0,0,0] },
		"c":  { type: "f", value: 0.5 },
		"p": { type: "f", value: 1.0}
	},

	vertexShader: [


		"varying float intensity;",
		"void main() {",
			"vec3 vNormal = normalize( normalMatrix * normal );",
			"vec3 vNormel = normalize( normalMatrix * viewVector );",
			"intensity = pow( c - dot(vNormal, vNormel), p );",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
		"}"
	].join("\n"),


	fragmentShader: [


		"uniform vec3 glowColor;",
		"varying float intensity;",
		"void main() {",
			"vec3 glow = glowColor * intensity;",
			"gl_FragColor = vec4( glow, 1.0 );",
		"}"
	].join("\n")

};
