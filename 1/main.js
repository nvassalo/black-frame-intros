/**
 * Created by nelsonvassalo on 11/01/17.
 */


// function SquareGenerator(w,h,posX, posY, lifetime) {
//     var w = w,
//         h = h,
//         posX = posX,
//         posY = posY,
//         lifetime = lifetime;
//
//
//
// }

var camera,
    renderer,
    geometry,
    material,
    mesh,
    uniforms,
    composer,
    renderPass,
    lightPass,
    copyPass,
    customMaterial,
    moonGlow,
    container,
    maskPass;




uniforms = {
    time: { type: 'f', value: 1.0 }
};

init();



function init() {

    var SCREEN_WIDTH = window.innerWidth,
        SCREEN_HEIGHT = window.innerHeight;

    container = $('body');

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(90, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 2000);
    camera.position.z = -300;
    scene.background = "#dddddd";
    scene.add(camera);
    camera.lookAt(scene.position);



    var ambientLight = new THREE.AmbientLight(0xdddddd, 0.5);
    scene.add(ambientLight);

    var floorMaterial = new THREE.MeshBasicMaterial({color: 0x440000});
    var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
    var floor = new THREE.Mesh(floorGeometry, floorMaterial);
    floor.position.y = 0;
    floor.rotation.x = Math.PI / 2;

    scene.add(floor);

    geometry = new THREE.CylinderGeometry(2, 2, 300, 32);

    // var debugMaterial = new THREE.MeshNormalMaterial();

    material = new THREE.MeshPhongMaterial({color:0x0000FF, shininess: 10});

    mesh = new THREE.Mesh(geometry, material);

     mesh.position.set(0,0,-10);
     mesh.rotation.x = 10;


    scene.add(mesh);


    customMaterial = new THREE.ShaderMaterial(
        {
            uniforms:
            {
                "color":   { type: "f", value: 0.4 },
                "power":   { type: "f", value: 4.6 },
                glowColor: { type: "c", value: new THREE.Color(0x4444cc) },
                viewVector: { type: "v3", value: camera.position }
            },
            vertexShader:   document.getElementById( 'vertexShader'   ).textContent,
            fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
            side: THREE.FrontSide,
            blending: THREE.NormalBlending,
            transparent: true
        }   );


    moonGlow = new THREE.Mesh( geometry.clone(), customMaterial.clone() );
    moonGlow.position = mesh.position;
    moonGlow.position.set(0,0,-10);
    moonGlow.rotation.x = mesh.rotation.x;

    // moonGlow.position.y = mesh.position.y;
    // moonGlow.position.z = mesh.position.z;
    moonGlow.scale.multiplyScalar(1.25);
    scene.add( moonGlow );




    var hblur = new THREE.ShaderPass( THREE.ShaderExtras["horizontalBlur"] );
    var vblur = new THREE.ShaderPass( THREE.ShaderExtras[ "verticalBlur" ] );
    var bluriness = 3;

    hblur.uniforms[ "h" ].value = bluriness / SCREEN_WIDTH;
    vblur.uniforms[ "v" ].value = bluriness / SCREEN_HEIGHT;


    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.autoClear = false;

    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    container.append(renderer.domElement);



    //
    //



    lightPass = new THREE.ShaderPass(THREE.LaserShader);
    copyPass = new THREE.ShaderPass( THREE.CopyShader );

    renderPass = new THREE.RenderPass(scene, camera);
    maskPass = new THREE.ShaderPass(THREE.MaskPass);


    composer = new THREE.EffectComposer(renderer);
    composer.addPass(hblur);
    composer.addPass(vblur);
    composer.addPass(lightPass);
    composer.addPass(copyPass);
    composer.addPass(maskPass);
    composer.addPass(renderPass);

    copyPass.renderToScreen = true;





    animate();

}

function animate() {

    requestAnimationFrame(animate);
    render(1);

}

function render(delta) {
    // renderer.render(scene, camera);
    composer.render(scene, camera);


    mesh.rotation.x += 0.01;
    mesh.rotation.z += 0.02;
    moonGlow.rotation.x += 0.01;
    moonGlow.rotation.z  += 0.02;

    uniforms.time += delta * 10;
    moonGlow.material.uniforms.viewVector.value = new THREE.Vector3().subVectors( camera.position, moonGlow.position );


}
