/**
 * Created by nelsonvassalo on 11/01/17.
 */
var SCREEN_WIDTH = window.innerWidth;
var SCREEN_HEIGHT = window.innerHeight;
var container;
var camera, scene, glowscene;
var renderer, renderTarget, renderTargetGlow;
var mesh, zmesh, geometry, pointLight, pmesh;
var finalcomposer, glowcomposer, hblur, vblur;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var render_canvas = 1, render_gl = 1;
var has_gl = 0;
document.addEventListener( 'mousemove', onDocumentMouseMove, false );
function init() {
    container = document.createElement('div');
    document.body.appendChild(container);
    // MAIN SCENE

    camera = new THREE.PerspectiveCamera(75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 100000);
    camera.position.z = 140;
    scene = new THREE.Scene();
    scene.add(new THREE.AmbientLight(0xffffff));
    pointLight = new THREE.PointLight(0xffffff);
    pointLight.position.set(0, 100, 0);
    scene.add(pointLight);
    // RENDERER

    renderer = new THREE.WebGLRenderer({
        //antialias: true
    });
    renderer.autoClear = false;
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.domElement.style.position = "relative";
    container.appendChild(renderer.domElement);
    has_gl = 1;

    // LOADER


    var geometry = new THREE.CylinderGeometry(2, 2, 300, 32);

    // var debugMaterial = new THREE.MeshNormalMaterial();

    var material = new THREE.MeshPhongMaterial({color: 0x0000FF, shininess: 10});

    var mesh = new THREE.Mesh(geometry, material);

    mesh.position.set(0, 0, -10);
    mesh.rotation.x = 10;


    glowscene = new THREE.Scene();
    glowscene.add(new THREE.AmbientLight(0xffffff));
    glowcamera = new THREE.PerspectiveCamera(75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 100000);
    glowcamera.position = camera.position;
    // GLOW COMPOSER
    var renderTargetParameters = {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.LinearFilter,
        format: THREE.RGBFormat,
        stencilBufer: false
    };
    renderTargetGlow = new THREE.WebGLRenderTarget(SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParameters);

    var effectFXAA = new THREE.ShaderPass(THREE.ShaderExtras["fxaa"]);
    effectFXAA.uniforms['resolution'].value.set(1 / SCREEN_WIDTH, 1 / SCREEN_HEIGHT);
    hblur = new THREE.ShaderPass(THREE.ShaderExtras["horizontalBlur"]);
    vblur = new THREE.ShaderPass(THREE.ShaderExtras["verticalBlur"]);
    var bluriness = 3;
    hblur.uniforms['h'].value = bluriness / SCREEN_WIDTH;
    vblur.uniforms['v'].value = bluriness / SCREEN_HEIGHT;
    var renderModelGlow = new THREE.RenderPass(glowscene, glowcamera);
    glowcomposer = new THREE.EffectComposer(renderer, renderTargetGlow);
    glowcomposer.addPass(renderModelGlow);
    glowcomposer.addPass(hblur);
    glowcomposer.addPass(vblur);
    glowcomposer.addPass(hblur);
    glowcomposer.addPass(vblur);
    // FINAL COMPOSER

    var finalshader = {
        uniforms: {
            tDiffuse: {type: "t", value: 0, texture: null},
            tGlow: {type: "t", value: 1, texture: null}
        },
        vertexShader: [
            "varying vec2 vUv;",
            "void main() {",
            "vUv = vec2( uv.x, 1.0 - uv.y );",
            "gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "}"
        ].join("\n"),
        fragmentShader: [
            "uniform sampler2D tDiffuse;",
            "uniform sampler2D tGlow;",
            "varying vec2 vUv;",
            "void main() {",
            "vec4 texel = texture2D( tDiffuse, vUv );",
            "vec4 glow = texture2D( tGlow, vUv );",
            "gl_FragColor = texel + vec4(0.5, 0.75, 1.0, 1.0) * glow * 2.0;",
            "}"
        ].join("\n")
    };
    finalshader.uniforms['tGlow'].texture = glowcomposer.renderTarget2;

    var renderModel = new THREE.RenderPass(scene, camera);
    var finalPass = new THREE.ShaderPass(finalshader);
    finalPass.needsSwap = true;
    finalPass.renderToScreen = true;
    renderTarget = new THREE.WebGLRenderTarget(SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParameters);
    finalcomposer = new THREE.EffectComposer(renderer, renderTarget);
    finalcomposer.addPass(renderModel);
    finalcomposer.addPass(effectFXAA);
    finalcomposer.addPass(finalPass);

    zmesh = new THREE.Mesh(geometry, new THREE.MultiMaterial());
    zmesh.position.set(0, 0, 0);
    zmesh.scale.set(3, 3, 3);
    zmesh.overdraw = true;
    scene.add(zmesh);
    glowscene.add(mesh);

}

function onDocumentMouseMove(event) {
    mouseX = ( event.clientX - windowHalfX );
    mouseY = ( event.clientY - windowHalfY );
}
//
function animate() {
    requestAnimationFrame( animate );
    render();
}
var t = 0.0;
function render() {
    t += .01;
    camera.position.x += ( mouseX/2 - camera.position.x ) * .05;
    camera.position.y += ( - mouseY/2 - camera.position.y ) * .05;
    camera.lookAt( scene.position );
    glowcamera.lookAt( scene.position );
    pointLight.position.set( 0, Math.cos(t)*200, 0 );
    glowcomposer.render( 0.1 );
    finalcomposer.render( 0.1 );
}
init();
animate();