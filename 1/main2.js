/**
 * Created by nelsonvassalo on 11/01/17.
 */
var camera, scene, renderer;
var cubeHolder;

var dotMatrixPass, dotMatrixParams;
var hblurPass;
var vblurPass;
var fxaaPass,
    glowComposer,
    renderPass;

var screenW, screenH;


init();
animate();

function init() {

    screenW = window.innerWidth;
    screenH = window.innerHeight;

    //init three scene
    camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 3000);
    camera.position.z = -300;
    scene = new THREE.Scene();
    scene.background = "#dddddd";
    scene.add(camera);
    camera.lookAt(scene.position);




    geometry = new THREE.CylinderGeometry(200, 200, 30, 32);

     var debugMaterial = new THREE.MeshNormalMaterial();

    material = new THREE.MeshBasicMaterial(0x0000FF);

    mesh = new THREE.Mesh(geometry, material);

    mesh.position.set(0,0, 10);
    mesh.rotation.x = 10;


    scene.add(mesh);


    var ambientLight = new THREE.AmbientLight(0xffffff, 1);
    scene.add(ambientLight);




    //init renderer
    renderer = new THREE.WebGLRenderer({});
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );


    // POST PROCESSING

    //common render target params
    var renderTargetParameters = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBFormat, stencilBufer: false };

    //Init dotsComposer to render the dots effect
    //A composer is a stack of shader passes combined

    //a render target is an offscreen buffer to save a composer output
    renderTargetDots = new THREE.WebGLRenderTarget( screenW, screenH, renderTargetParameters );
    //dots Composer renders the dot effect
    dotsComposer = new THREE.EffectComposer( renderer,renderTargetDots );

    var renderPass = new THREE.RenderPass( scene, camera );
    //a shader pass applies a shader effect to a texture (usually the previous shader output)
    dotMatrixPass = new THREE.ShaderPass( THREE.DotMatrixShader );
    dotsComposer.addPass( renderPass );
    dotsComposer.addPass( dotMatrixPass );


    //Init glowComposer renders a blurred version of the scene
    renderTargetGlow = new THREE.WebGLRenderTarget( screenW, screenH, renderTargetParameters );
    glowComposer = new THREE.EffectComposer( renderer, renderTargetGlow );

    //create shader passes
    hblurPass = new THREE.ShaderPass( THREE.HorizontalBlurShader );
    vblurPass = new THREE.ShaderPass( THREE.VerticalBlurShader );

    glowComposer.addPass( renderPass );
    glowComposer.addPass( dotMatrixPass );
    glowComposer.addPass( hblurPass );
    glowComposer.addPass( vblurPass );
    //glowComposer.addPass( fxaaPass );

    //blend Composer runs the AdditiveBlendShader to combine the output of dotsComposer and glowComposer
    blendPass = new THREE.ShaderPass( THREE.AdditiveBlendShader );
    blendPass.uniforms[ 'tBase' ].value = dotsComposer.renderTarget1;
    blendPass.uniforms[ 'tAdd' ].value = glowComposer.renderTarget1;
    blendComposer = new THREE.EffectComposer( renderer );
    blendComposer.addPass( blendPass );
    blendPass.renderToScreen = true;

    //////////////

    //Init DAT GUI control panel
    dotMatrixParams = {
        spacing: 20.0,
        size: 2.0,
        blur: 3.0
    };

    glowParams = {
        amount: 4.0,
        blur: 0.4
    };


    //Init DAT GUI control panel

    glowParams = {
        amount: 5.5,
        blur: 0.8
    };

    hblurPass.uniforms[ 'h' ].value = glowParams.blur / screenW*2;
    vblurPass.uniforms[ 'v' ].value = glowParams.blur  / screenH*2;
    blendPass.uniforms[ 'amount' ].value = glowParams.amount  / screenH*2;

}





function animate() {


    requestAnimationFrame( animate );

    render();
}

function render() {
    glowComposer.render( 0.1 );
    mesh.rotation.x += 0.01;
    mesh.rotation.z += 0.02;
}