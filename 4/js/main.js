var wW = window.innerWidth,
    wH = window.innerHeight,
    logo,
    panX;/**
 * Created by nelsonvassalo on 20/01/17.
 */



function preload() {  // preload() runs once
    logo = loadImage('/img/logo.png');
}

function setup() {
    createCanvas(wW, wH, WEBGL);
    // smooth();
    // system = new ParticleSystem(createVector(width/2, 50));
    var fov = 80 / 180 * PI;
    var cameraZ = (height/2.0) / tan(fov/2.0);
    perspective(60 / 180 * PI, width/height, cameraZ * 1.1, cameraZ * 5);
}



function draw() {
    background(25);
    rotateY(panX);

    for(var i = 0; i < 40; i++){

        translate(0, sin(frameCount * 0.008 + i) * wH, cos(frameCount * 0.008 + i) * wH);
        push();
        ambientMaterial(0,25);
        texture(logo);
        tint(0,25.5);
        plane(537, 72,2,2);
        pop();
    }

}

function mouseMoved() {
    panX = 0 || (mouseX - (wW/2 - mouseX)) / 7200;
}

function mousePressed() {

}

