var Particle = function(position) {
    this.lifespan = 255.0;
    this.origin = position;
};

Particle.prototype.run = function() {
    this.update();
    this.display();
};

// Method to update position
Particle.prototype.update = function(){
    this.lifespan -= 2;
    translate(0, cos(frameCount * 0.008) * height, sin(frameCount * 0.008) * 400);
};

// Method to display
Particle.prototype.display = function() {

    ambientMaterial(0,this.lifespan);
    texture(logo);
    tint(0,this.lifespan);
    plane(537, 72,2,2);
};

// Is the particle still useful?
Particle.prototype.isDead = function(){
    if (this.lifespan < 0) {
        return true;
    } else {
        return false;
    }
};

var ParticleSystem = function(position) {
    this.origin = position;
    this.particles = [];
};

ParticleSystem.prototype.addParticle = function() {
    this.particles.push(new Particle(this.origin));
};

ParticleSystem.prototype.run = function() {
    for (var i = this.particles.length-1; i >= 0; i--) {
        var p = this.particles[i];
        p.run();
        if (p.isDead()) {
            this.particles.splice(i, 1);
        }
    }
};