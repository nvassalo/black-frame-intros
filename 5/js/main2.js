var wW = window.innerWidth,
    wH = window.innerHeight,
    logo,
    panX,
    system;/**
 * Created by nelsonvassalo on 20/01/17.
 */



function preload() {  // preload() runs once
    logo = loadImage('/img/logo.png');
}

function setup() {
    createCanvas(wW, wH, WEBGL);
    // smooth();
    // system = new ParticleSystem(createVector(width/2, 50));
    var fov = 160 / 180 * PI;
    var cameraZ = (height/2.0) / tan(fov/2.0);
    perspective(60 / 180 * PI, width/height, cameraZ * 0.01, cameraZ * 100);
    system = new ParticleSystem(0,0,0);
}



function draw() {
    background(25);
    // rotateY(panX);
    orbitControl();
    ambientLight(255,255,255);

    system.addParticle();
    system.run();
}
