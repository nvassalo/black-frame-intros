var container;
var camera, scene, renderer;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var material;
var light, geometry, solid, solid2, solid3, solid4;

init();
animate();

function init() {

    container = document.getElementsByTagName( 'body' )[0];
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 10000 );
    camera.position.z = 1000;
    scene = new THREE.Scene();
    scene.fog = new THREE.Fog(0x000000, 0, 5000);
    light = new THREE.PointLight(0x6d6d6d, 30, 1000);
    light.position.set( 600, 0, 1000 );
    light.rotateY(-60);

    var light2 = new THREE.PointLight(0xffffff, 10, 2000);
    light2.position.set(-600, 0, 1000);
    light2.rotateX(-15);
    light2.rotateY(-30);

    scene.add( light );
    scene.add(light2);

    var texloader = new THREE.CubeTextureLoader();
    var txload2 = new THREE.CubeTextureLoader();

    var textureCube = texloader.load( [
        'img/bigLogo.png',
        'img/bigLogo.png',
        'img/bigLogo.png',
        'img/bigLogo.png',
        'img/bigLogo.png',
        'img/bigLogo.png'
    ] );


    textureCube.mapping = THREE.CubeRefractionMapping;

    var textureCube2 = txload2.load( [
        'img/several_glows.jpg',
        'img/several_glows.jpg',
        'img/several_glows.jpg',
        'img/several_glows.jpg',
        'img/several_glows.jpg',
        'img/several_glows.jpg'
        ]);

    textureCube2.mapping = THREE.CubeRefractionMapping;



    var logo = new THREE.TextureLoader().load( 'img/logo.png' );



    // bigLogo.depthWrite = false;

    logo.wrapS = logo.wrapT = THREE.RepeatWrapping;
    logo.repeat.set( 1, 1 );

    geometry = new THREE.TetrahedronGeometry(800, 2);

    var geometry2 = new THREE.TetrahedronGeometry(800,2);
    var geometry3 = new THREE.TetrahedronGeometry(650, 1);
    var geometry4 = new THREE.TetrahedronGeometry(600, 1);

    material = new THREE.MeshPhongMaterial({
        color: 0x000009,
        shininess: 10,
        specular: 0x0F0600,
        emissive: 0x000000,
        shading: THREE.FlatShading,
        opacity: 1,
        envMap: textureCube,
        refractionRatio: 0.4,
        reflectivity: 30,
        blending: THREE.AdditiveBlending
    });

    var material2 = new THREE.MeshPhongMaterial({
        color: 0x000C0B,
        metalness: 0.2,
        // emissive: 0x222222,
        shading: THREE.FlatShading,
        specular: 0x0A000D,
        opacity: 0.75,
        envMap: textureCube,
        transparent: true,
        refractionRatio: 0.8,
        reflectivity: 35,
        shininess: 70,
        side: THREE.DoubleSide
    });

    material.depthWrite = false;

    solid = new THREE.Mesh(geometry, material);
    solid.position.set(900, 200 , 0);


    //left solid
    solid2 = new THREE.Mesh(geometry2, material);
    solid2.position.set(-400, 0, -100);

    solid3 = new THREE.Mesh(geometry3, material2);
    solid3.position.set(500, 0, -300);

    solid4 = new THREE.Mesh(geometry4, material2);
    solid4.position.set(-200, 0, 0);

    scene.add(solid);
    scene.add(solid2);
    scene.add(solid3);
    scene.add(solid4);


    //537 × 72

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setClearColor( 0x000000 );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.sortObjects = false;
    container.appendChild( renderer.domElement );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'touchstart', onDocumentTouchStart, false );
    document.addEventListener( 'touchmove', onDocumentTouchMove, false );
    //
    window.addEventListener( 'resize', onWindowResize, false );

    window.scene = this.scene;

}
function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}


function onDocumentMouseMove( event ) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
}
function onDocumentTouchStart( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}
function onDocumentTouchMove( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}
//
function animate() {
    requestAnimationFrame( animate );
    render();
}
function render() {
    // TWEEN.update();
    camera.position.x += ( mouseX - camera.position.x ) * 0.002;
    camera.position.y += ( - mouseY - camera.position.y ) * 0.005;
    camera.lookAt( scene.position );
    solid.rotation.x += 0.001;
    solid.rotation.y += 0.001;
    solid.rotation.z += 0.001;
    solid2.rotation.x -= 0.001;
    solid2.rotation.y -= 0.001;
    solid2.rotation.z -= 0.001;
     solid3.rotation.x -= 0.001;
    solid3.rotation.y -= 0.001;
    solid3.rotation.z -= 0.001;
    solid4.rotation.x += 0.001;
    solid4.rotation.y += 0.001;
    solid4.rotation.z += 0.001;
    renderer.render( scene, camera );
}