var container, stats;
var camera, scene, renderer, particle;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var material;
var _color;

init();
animate();

function init() {
    container = document.getElementsByTagName( 'body' )[0];
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.z = 600;
    scene = new THREE.Scene();
    scene.fog = new THREE.Fog(0x000000, -2000, 2000);
    _color = new THREE.Color(0xffff00);

    var logo = new THREE.TextureLoader().load( 'img/logo.png' );
    material = new THREE.SpriteMaterial( {
        map: logo,
        fog: true,
        blending: THREE.CustomBlending,
        blendEquation: THREE.AddEquation, //default
        blendSrc: THREE.DstAlphaFactor, //default
        blendDst: THREE.OneMinusSrcColorFactor, //default
        color: 0xff4512,
        opacity: true
    } );

    bigLogo = new THREE.SpriteMaterial({
        map: logo
    });


    var centerLogo = new THREE.Sprite(bigLogo);

    scene.add(centerLogo);


    centerLogo.position.set(0,0,0);
    centerLogo.material.opacity = 1;
    centerLogo.scale.x = 512;
    centerLogo.scale.y = 64;


    var curve = new THREE.CatmullRomCurve3( [
        new THREE.Vector3( 0, 0, 0 ),
        new THREE.Vector3( 50, -50, 0 ),
        new THREE.Vector3( 0, -55, 0 ),
        new THREE.Vector3( 0, -60, 0 ),
        new THREE.Vector3( 0, -80, 0 )
    ] );


    for ( var i = 0; i < 30; i++ ) {
        particle = new THREE.Sprite( material );
        particle.fog = true;
        initParticle( particle, i * (200/3) );
        scene.add( particle );
    }


    var path = new THREE.Path( curve.getPoints( 50 ) );

    var geometry = path.createPointsGeometry( 50 );
    var material = new THREE.MeshBasicMaterial( 0xff00ff);

// Create the final object to add to the scene
    var splineObject = new THREE.Line( geometry, material );

    scene.add(splineObject);


    //537 × 72

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setClearColor( 0x2a003a );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );
    stats = new Stats();
    container.appendChild( stats.dom );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'touchstart', onDocumentTouchStart, false );
    document.addEventListener( 'touchmove', onDocumentTouchMove, false );
    //
    window.addEventListener( 'resize', onWindowResize, false );
}
function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function initParticle( particle, delay ) {
    var particle = this instanceof THREE.Sprite ? this : particle;
    var delay = delay !== undefined ? delay : 0;
    particle.position.set( 0, 0, 0 );
    particle.scale.x = 512;
    particle.scale.y = 64;
    particle.fog = true;
    particle.transparent = true;
    new TWEEN.Tween( particle )
        .delay( delay )
        .to( {}, 2000 )
        .onComplete( initParticle )
        .start();
    new TWEEN.Tween( particle.position )
        .delay( delay )
        .to( { x: mouseX * 3, y: -mouseY * 3, z: -1400 }, 2000 )
        .easing(TWEEN.Easing.Cubic.In)
        .start();
    // new TWEEN.Tween( particle)
    //     .delay(delay)
    //     .to({opacity: 0}, 30 )
    //     .start();

    // new TWEEN.Tween( particle.scale )
    //     .delay( delay )
    //     .to( { x: 0.01, y: 0.01 }, 1000 )
    //     .start();
}
//
function onDocumentMouseMove( event ) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
}
function onDocumentTouchStart( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}
function onDocumentTouchMove( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}
//
function animate() {
    requestAnimationFrame( animate );
    render();
    stats.update();
}
function render() {
    TWEEN.update();
    // camera.position.x += ( mouseX - camera.position.x ) * 0.05;
    // camera.position.y += ( - mouseY - camera.position.y ) * 0.02;
    // camera.lookAt( scene.position );
    renderer.render( scene, camera );
}