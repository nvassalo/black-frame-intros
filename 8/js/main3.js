var container, stats;
var camera, scene, renderer, particle;
var mouseX = 0, mouseY = 0;
var wW = window.innerWidth,
    windowHalfX = wW / 2;
var wH = window.innerHeight,
    windowHalfY = wH / 2;
var material, _material;
var _color, _colors, ii = 0;
var scaleX, scaleY;



document.addEventListener("DOMContentLoaded", function () {
    init();
    createParticles();
    animate();
});


function init() {
    container = document.getElementsByTagName( 'body' )[0];
    camera = new THREE.PerspectiveCamera( 75, wW / wH, 5, 10000 );
    camera.position.z = 600;
    scene = new THREE.Scene();
    scene.fog = new THREE.Fog(0x000000, -400, 1200);
    _color = new THREE.Color(0xffff00);

    var frame = new THREE.TextureLoader().load( 'img/frame.png' );

    material = new THREE.SpriteMaterial( {
        map: frame,
        fog: true,
        blending: THREE.AdditiveBlending,
        // blendEquation: THREE.AddEquation,
        // blendSrc: THREE.DstAlphaFactor,
        // blendDst: THREE.OneMinusSrcColorFactor,
        color: 0xff4512,
        opacity: true
    } );


    _colors = [
        new THREE.Color(0xFF8200),
        new THREE.Color(0xFF6700),
        new THREE.Color(0xff0000),
        new THREE.Color(0xFF6700)
    ];


    //537 × 72

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setClearColor( 0x000000 );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( wW, wH );
    container.appendChild( renderer.domElement );
    stats = new Stats();
    container.appendChild( stats.dom );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'touchstart', onDocumentTouchStart, false );
    document.addEventListener( 'touchmove', onDocumentTouchMove, false );
    window.addEventListener('resize', onWindowResize, false);
}


function returnColor(ii) {
    if (ii >= _colors.length) {
        return _colors[0];
    } else {
        return _colors[ii];
    }
}


function createParticles() {
    // SYNCHRONOUS lESS PARTICLES

    setInterval(function () {
        var particle = new THREE.Sprite(_material);
        initParticle(particle);

    }, 1000);

    setInterval (function() {
        if ( ii < _colors.length) {
            ii++;
        } else {
            ii=0;
        }
    }, 4000);

}
function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function initParticle( particle ) {
    var particle = this instanceof THREE.Sprite ? this : particle;
    particle.fog = true;
    scene.add( particle );
    particle.material = _material;
    particle.position.set( 0, 0, -1000 );
    scaleX = particle.scale.x = (wW/100) * 35;
    scaleY = scaleX / 1.484496124;

    particle.scale.y = scaleY;

    particle.fog = true;
    particle.transparent = true;
    _material.color = returnColor(ii);
    console.log(ii);

    // console.log(ii + "--", _material.color);

}
//
function onDocumentMouseMove( event ) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
}
function onDocumentTouchStart( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}
function onDocumentTouchMove( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}
//
function animate() {
    requestAnimationFrame( animate );
    render();
    stats.update();
}

function render() {


    for ( var i = 0; i < scene.children.length - 1; i++ ) {
        var p = scene.children[i];
        var ps = p.position, x, y, z;
        var sx = p.scale.x;
        var sy = p.scale.y;
        if ( ps ) {
            x = ps.x + (mouseX * 0.001);
            y = ps.y - (mouseY * 0.001);
            z = ps.z + (25+i)* Math.abs(Math.pow(0.085,1.000001));
            sx = scaleX + i* 15;
            sy = scaleY + i * 15;


            if ( z >= 700) {
                scene.remove( p );
                // z *= -1;
            }
            else{
                p.position.set(x , y, z );
                // p.scale.set(sx,sy);
            }

        }

    }
    _material = material.clone();

    renderer.render( scene, camera );
}